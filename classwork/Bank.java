/* Bank class contains the main method.
It creates two instances of the Account object and uses
the method within the Account class
*/

import java.util.Scanner;
public class Bank
{
    private class Account();

    public static void main ( String args[] )
    {
	// create instances of Account
        Account a1 = new Account ( 50.00 );
        Account a2 = new Account ( -7.53 );
	// Call/invoke getBalance method in Account class
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );

        Scanner input = new Scanner ( System.in );
	// Get an input from the user
        System.out.print ( "Enter deposit for a1: ");
        double depositAmount = input.nextDouble();
        System.out.println ( "Adding "+ depositAmount+ " to a1" );
	// call/invoke credit method in Account class
        a1.credit ( depositAmount );

	// Call/invoke getBalance method in Account class
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );

	// Get an input from the user
        System.out.print ( "Enter deposit for a2: ");
        depositAmount = input.nextDouble();
        System.out.println ( "Adding "+ depositAmount+" to a2" );
	// Call/invoke credit method in Account class
        a2.credit ( depositAmount );
	// Call/invoke getBalance method in Account class
        System.out.println ("a1 balance: "+ a1.getBalance() );
        System.out.println ("a2 balance: "+ a2.getBalance() );
    }
}

