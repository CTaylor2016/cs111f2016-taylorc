//Chris Taylor
//09/26/2016

import java.util.Scanner;

public class StringExample

{
    public static void main(String args [])
    {
        //declarations/assignments
        String word = ""; //empty string
        int length = 0;
        char c;
        int index;
        Scanner userInput = new Scanner (System.in);

        //getting input
        System.out.println("Enter a string.");
        word = userInput.nextLine();

        System.out.println(word);

        //String length-getting
        length = word.length();
        System.out.println("Length: "+length);
        c = word.charAt(3);
        index = word.indexOf('a');
        System.out.println("The third character is "+c+" and the letter 'a' appears at "+index+".");
    }
}
