//Chris Taylor
//11-9-2016

import java.util.Scanner;

public class SwitchVowel
{
    public static void main(String args[])
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a character.");
        char character = in.next().charAt(0);

        switch ( character )
        {
            case 'a': case 'A': // case labels
            case 'e': case 'E': // separated by :
            case 'i': case 'I': // character
            case 'o': case 'O': // notice use of ‘ ’
            case 'u': case 'U': // marks for char tests
                System.out.print (character+" is a vowel\n");
            break;
            default:
                System.out.print (character+" is not a vowel\n");
        }
    }
}
