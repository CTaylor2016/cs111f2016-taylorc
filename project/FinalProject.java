//*************************************
// Honor Code: This work is mine unless otherwise cited.
// Christopher Taylor
// CMPSC 111 Fall 2016
// Final Project
// 12/2/2016
//
// Purpose: To create a simple survival game and demonstrate understanding of class material
//*************************************

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FinalProject
{
    //Main method begins
    public static void main(String[] args)
    {
        Resources stuff = new Resources();
        Dialogue words = new Dialogue(stuff);

        //creating game frame
        JFrame game=new JFrame();
        //Setting size/layout of game
        game.setLayout(new GridLayout(7, 3));
        game.setSize(1200, 800);
        game.setVisible(true);
        game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //buttons/labels
        JLabel sustenance = new JLabel();
        JLabel violence = new JLabel();
        JLabel defense = new JLabel();
        JLabel time = new JLabel();
        JLabel riskAnalysis = new JLabel();
        JButton huntB = new JButton("Hunt Zombies");
        JButton ammoB = new JButton("Gather Ammo");
        JButton foodB = new JButton("Gather Food");
        JButton eatB = new JButton("Eat");
        JButton buildB = new JButton("Gather Material");
        JButton fortB = new JButton("Fortify");
        JButton seekB = new JButton("Seek New Shelter");
        JButton helpB = new JButton("Help");
        //First row
        game.add(new JLabel(""));
        game.add(new JLabel("Zombies or something"));
        game.add(new JLabel(""));
        //Second row
        game.add(ammoB);
        game.add(huntB);
        game.add(violence);
        //Third row
        game.add(foodB);
        game.add(eatB);
        game.add(sustenance);
        //Fourth row
        game.add(buildB);
        game.add(fortB);
        game.add(defense);
        //Fifth row
        game.add(time);
        game.add(seekB);
        game.add(riskAnalysis);
        //Sixth row
        game.add(new JLabel(""));
        game.add(new JLabel(""));
        game.add(new JLabel(""));
        //Seventh row
        game.add(new JLabel(""));
        game.add(helpB);
        game.add(new JLabel(""));
        //button effects
        huntB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.hunting();
                words.zKill();
            }});
        ammoB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.ammoSearch();
                words.ammoGet();
            }});
        foodB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.foodSearch();
                words.foodGet();
            }});
        eatB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.eatFood();
                words.foodEat();
            }});
        buildB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.buildSearch();
                words.buildGet();
            }});
        fortB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.buildWall();
                words.wallBuild();
            }});
        seekB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                stuff.newLoc();
                words.campFind();
            }});
        helpB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                words.helpWindow();
            }});
        //initial array set
        words.saveNums();
        //forcing refresh
        while(stuff.getHealth()>0)
        {
            //formatting
            violence.setText("<html>"+"You have "+stuff.getAmmo()+" rounds of ammo. There are "+stuff.getZombies()+" zombies in the area."+"</html>");
            defense.setText("<html>"+"Your defenses are "+stuff.getWallAnalysis()+" against the zombie hordes. You have " +stuff.getMaterial()+" pounds of building material."+"</html>");
            time.setText("<html>"+"It is "+(stuff.getHours()+6)+":00 on Day "+stuff.getDays()+"."+"</html>");
            sustenance.setText("<html>"+"You have food for "+stuff.getFood()+" days."+"\n"+"You are "+stuff.getHealthAnalysis()+"."+"</html>");
            riskAnalysis.setText("<html>"+stuff.getRiskAnalysis()+"</html>");

            //day checker
            if(stuff.getHours()>11)
            {
                stuff.daychange();
                words.saveNums();
            }

            //life checker
            if(stuff.getHealth()<1)
            {
                words.gameOver();
            }
        }

    }
}
