package lab9;

import lejos.hardware.Button;
import lejos.hardware.Battery;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;
import lejos.utility.PilotProps;
import lejos.hardware.Device;
import lejos.hardware.sensor.BaseSensor;
import lejos.hardware.sensor.UARTSensor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import java.util.Iterator;
import lejos.hardware.Sound;

public class RoboShenanigans {
	
	//declarations
	
	private PilotProps pp;
	private RegulatedMotor leftMotor, rightMotor;
	private float wheelDiameter;
	private float trackWidth;
	private boolean reverse;
	private DifferentialPilot robot;
	private float volts;
	private EV3UltrasonicSensor sonicSensor;
	private SampleProvider sonicSamplePr;

	float[] sonicSample;
	
	//constructor
	public RoboShenanigans(float[] s, SampleProvider sp)
	{
		//Declaring sensor and port
		//EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S3);	
		//SampleProvider sonicSamplePr = sonicSensor.getDistanceMode();
		// Set the appropriate values for wheel diameter and track width
		pp = new PilotProps();
		// wheelDiameter - diameter of the tire
		wheelDiameter = Float.parseFloat(pp.getProperty(PilotProps.KEY_WHEELDIAMETER, "4"));
		// trackWidth - distance between center of right tire and center of left tire
		trackWidth = Float.parseFloat(pp.getProperty(PilotProps.KEY_TRACKWIDTH, "14.0"));
		leftMotor = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_LEFTMOTOR, "B"));
		rightMotor = PilotProps.getMotor(pp.getProperty(PilotProps.KEY_RIGHTMOTOR, "C"));
		reverse = Boolean.parseBoolean(pp.getProperty(PilotProps.KEY_REVERSE,"false"));
		// Create an instance of the robot
		// The DifferentialPilot is an abstraction of the Pilot mechanism of the robot.
		// It contains methods to control robot movements: travel forward or backward in a straight line
		// or a circular path or rotate to a new direction.
		robot = new DifferentialPilot(wheelDiameter,trackWidth,leftMotor,rightMotor,reverse);
		//sonicSample = new float[sonicSamplePr.sampleSize()];
		//objectDetector();
		//sonicSamplePr.fetchSample(sonicSample, 0);
		sonicSample = s;
		sonicSamplePr = sp;
	}
	public void patrol()
	{
		System.out.println("Hail Satan!");
		for(int count=0; count<4; count++)
		{
			// set the acceleration of the robot
			robot.setAcceleration(3000);
			// set the moving speed
			robot.setTravelSpeed(20); // cm/sec
			// set the rotation speed
			robot.setRotateSpeed(45); // deg/sec
			// move forward
			robot.forward();
			for(count=0; count<1; count++)
			{
				Delay.msDelay(100);
				checkVoltage();
				objectDetector();
			}
			System.out.println("Stopping");
			// stop
			robot.stop();
			System.out.println("Stopped");
			Delay.msDelay(2000);
			System.out.println("Turning right");
			// rotate right
			robot.rotateRight();
			Delay.msDelay(1000);
			//objectDetector();
		}
		System.out.println("Stopping");
		// stop
		robot.stop();
	}
		
	public void objectDetector()
	{
		sonicSamplePr.fetchSample(sonicSample, 0);
		if(sonicSample[0]< 0.1) //Stops robot if it's within 10cm
		{
			robot.quickStop();
			System.out.println("Oops! Object Detected");
			Sound.buzz();
			Delay.msDelay(2000);
			// move backwards
			robot.backward();
			Delay.msDelay(500);
			//turns to avoid crashing
			robot.rotateRight();
			Delay.msDelay(1000);
		}
	}
	
	//using a float in order to make it function as a variable in Lab9
	public void checkVoltage()
	{
		//getting the voltage pinned down as a variable
		float bat = Battery.getVoltage();
		if(bat<2)
		{
			robot.stop();
			System.out.println("Battery low");
			Sound.beep();
			Delay.msDelay(5000);
			System.exit(0);
		}
	}

}