import javax.swing.*;
import java.awt.*;

public class ActionListenerMain
{
    public static void main(String args[])
    {
        // create frame/window
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // automatically arranges components left to right
        frame.setLayout(new FlowLayout());

        // create a button
        JButton myButton = new JButton("My Button");

        // create a textfield for user's input
        JTextField myTextField = new JTextField("Type here");

        // create a label
        JLabel myLabel = new JLabel("My Label");

        myButton.addActionListener(new SetTextListener(myTextField, myLabel));


        frame.add(myButton);
        frame.add(myTextField);
        frame.add(myLabel);

        frame.setVisible(true);
    }
}
