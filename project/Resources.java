import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Resources

{

    //Variables
    private int a=200;
    private int f=7;
    private int h=75;
    private int hour=0;
    private int z=50;
    private int w=100;
    private int m=0;
    private int d=0;
    private int r;
    private int risk=1;
    Random rand = new Random();

    public int getAmmo()
    {
        return a;
    }

    public int getFood()
    {
        return f;
    }

    public int getHealth()
    {
        return h;
    }

    public String getHealthAnalysis()
    {
        //Possibilities
        String best = "in great condition";
        String good = "in good condition";
        String poor = "in poor condition";
        String bad = "in terrible condition";
        String dying = "on death's door";

        //Determining response
        if(h>75)
        {
            return best;
        }
        else if(h>50)
        {
            return good;
        }
        else if(h>30)
        {
            return poor;
        }
        else if(h>10)
        {
            return bad;
        }
        else
        {
            return dying;
        }
    }

    public int getZombies()
    {
        return z;
    }

    public int getWalls()
    {
        return w;
    }

    public String getWallAnalysis()
    {
        //Possible responses
        String strong = "a towering bulwark";
        String adequate = "a sturdy defense";
        String frail = "a crumbling barrier";
        String gone = "no defense at all";

        //Determining response
        if(w>150)
        {
            return strong;
        }
        else if(w>75)
        {
            return adequate;
        }
        else if(w>20)
        {
            return frail;
        }
        else
        {
            return gone;
        }
    }

    public int getMaterial()
    {
        return m;
    }

    public int getHours()
    {
        return hour;
    }

    public int getDays()
    {
        return d;
    }

    public int getDanger()
    {
        return risk;
    }

    public String getRiskAnalysis()
    {
        String justArrived = "You've only arrived at this place today. You haven't drawn any attention, and there's plenty to forage.";
        String fresh = "You haven't spent very long here. The area is as safe as anywhere can be, these days. You don't have any trouble finding food or material.";
        String increase = "There might be increased zombie presence, but it's hard to tell. There's still plenty of supplies around, but you're not sure how long that will last.";
        String trouble = "You're definitely attracting attention, and you've picked up most of the easy supplies, but you're probably good for a few more days.";
        String unsafe = "You should probably move on. You're beginning to pick this place clean, and the zombie presence is only getting stronger.";
        String leaveNow = "Get out of here while you still can.";

        if(risk<1)
        {
            return justArrived;
        }
        else if(risk<5)
        {
            return fresh;
        }
        else if(risk<10)
        {
            return increase;
        }
        else if(risk<15)
        {
            return trouble;
        }
        else if(risk<20)
        {
            return unsafe;
        }
        else
        {
            return leaveNow;
        }
    }

    public void daychange()
    {
        //zombie attack on walls
        d++;
        hour=0;
        for(int c = 0; c<z; c++)
        {
            if(w>0)
            {
                r=rand.nextInt(2);
                w=w-r;
            }
            else
            {
                r=rand.nextInt(2);
               h=h-(r+1);
            }
        }
        //food consumption
        if(f>0)
        {
            f=f-1;
            h=h+10;
            if(h>100)
            {
                h=100;
            }
        }
        else
        {
            h=h-20;
        }
        //danger increasing
        risk=risk+1+rand.nextInt(3);
        z=z+5+risk+rand.nextInt(10);

    }

    public void hunting()
    {
        if(a>50)
        {
            for(int c = 0; c<50; c++)
            {
                if(z>0)
                {
                    r=rand.nextInt(2);
                    z=z-r;
                    a=a-1;
                }
            }
        }
        else
        {
            for(int c = 0; c<a; c++)
            {
                if(z>0)
                {
                    r=rand.nextInt(2);
                    z=z-r;
                }
            }
            a=0;
        }
        hour=hour+2;
    }

    public void ammoSearch()
    {
        r=rand.nextInt(100);
        if((r-risk)>=80)
        {
            r=rand.nextInt(51);
            a=a+50+r;
        }
        else if((r-risk)>=20)
        {
            if((risk-r)<50)
            {
                r=rand.nextInt((z/5)+1);
                h=h-r;
            }
            r=rand.nextInt(51);
            a=a+r;
        }
        else
        {
            r=rand.nextInt((z/5)+1);
            h=h-r;
        }
        hour=hour+2;
    }

    public void foodSearch()
    {
        r=rand.nextInt(100);
        if((r-risk)>59)
        {
            f=f+1;
        }
        else if((r-risk)<50)
        {
            r=rand.nextInt((z/5)+1);
            h=h-r;
        }
        hour=hour+2;
    }

    public void eatFood()
    {
        if(f>0)
        {
            h=h+15;
            if(h>100)
                {
                    h=100;
                }
            f=f-1;
        }
        hour=hour+2;
    }

    public void buildSearch()
    {
        r=rand.nextInt(100);
        if((r-risk)>=75)
        {
            r=rand.nextInt(21);
            m=m+20+r;
        }
        else if((r-risk)>25)
        {
            if((r-risk)<51)
            {
                h=h-rand.nextInt((z/5)+1);
            }
            r=rand.nextInt(21);
            m=m+r;
        }
        else
        {
            h=h-rand.nextInt((z/5)+1);
        }
        hour=hour+2;
    }

    public void buildWall()
    {
        r=rand.nextInt(100);
        if(m>30)
        {
            m=m-30;
            w=w+25;
            if(r>89)
            {
                w=w+15;
            }
            else if(r>74)
            {
                w=w+5;
            }
            if(r>10)
            {
                w=w-10;
            }
            else if(r>25)
            {
                w=w-5;
            }
        }
        else
        {
            w=w+(3*m/4);
            m=0;
            if(r>89)
            {
                w=w+5;
            }
            else if(r>74)
            {
                w=w+1;
            }
            if(r>10)
            {
                w=w-3;
            }
            else if(r>25)
            {
                w=w-1;
            }
        }
        hour=hour+2;
    }
    public void newLoc()
    {
        risk=0;
        z=rand.nextInt(20)+41;
        w=0;
        if(m>80)
        {
            m=80;
        }
        if(f>10)
        {
            f=10;
        }
        if(a>500)
        {
            a=500;
        }
        hour=hour+6;
    }
}
