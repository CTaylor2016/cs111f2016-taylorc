import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SetTextListener implements ActionListener
{
    // instance variable
    private JLabel myLabel;
    private JTextField myTextField;

    // constructor
    public SetTextListener (JTextField textField, JLabel label)
    {
        myLabel = label;
        myTextField = textField;
    }

    public void actionPerformed (ActionEvent event)
    {
        // to change the label to the String inside text field
        myLabel.setText(myTextField.getText());
    }
}

