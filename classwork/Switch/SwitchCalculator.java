//Chris Taylor
//Nov. 9, 2016

public class SwitchCalculator
{
    private double result;

    public SwitchCalculator(double res)
    {
        result = res;
    }
    public void calculate(double l, char o, double r)
    {
        switch(o)
        {
            case '+': result = l+r;
                break;
            case '-': result = l-r;
                break;
            case '*': result = l*r;
                break;
            case '/': result = l/r;
                break;
            case '%': result = l%r;
                break;
            default: System.out.println("Unknown operator");
        }
    }
    public double getResult()
    {
        return result;
    }
}
