//*******************
// CMPSC 111, Class Exercise
// November 14, 2016
//
// Chris Taylor
// Purpose: Demonstrate the usage of loops and arrays to analyze names
//*********************
import java.io.*;
import java.util.Scanner;

public class Names
{
	public static void main(String args[]) throws IOException
	{

		String [] used = new String[50];

		File file = new File("names.txt");
		Scanner input = new Scanner(file);

		// read from the file and save values into an array
		String [] names = new String[50];
		int count = 0;

		while(input.hasNext())
		{
			String name = input.next();
			names[count] = name;
			count++;
		}

        //iterate through array
        for(int i = 0; i<names.length; i++)
        {
            if(names[i].startsWith("A"))
            {
                System.out.println(names[i]);
            }
        }
    }
}

