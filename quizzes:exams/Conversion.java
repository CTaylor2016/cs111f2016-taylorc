// Janyl Jumadinova
// Quiz 1 Sample, September 21
//
import java.util.Scanner;

public class Conversion
{
    public static void main (String[] args)
    {
        double tempF = 0; // declaration and assignment
       
        double tempC = 0; // declaration and assignment

        // declare Scanner object
        Scanner userInput = new Scanner(System.in);

        // prompt user for input
        System.out.println("Enter a temperature in Fahrenheit: ");
        tempF = userInput.nextDouble();

        System.out.println("You entered: "+tempF);

        // convert temp in F to temp in C
        tempC = (tempF - 32) * 5/9;
        System.out.println("Temperature in celcius is: "+tempC);
    }
}
