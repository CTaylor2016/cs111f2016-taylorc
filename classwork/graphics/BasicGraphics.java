//=================================================
// Class Example using Graphics
// November 21, 2016
// Janyl Jumadinova
//
// Purpose: Illustrate Graphics class methods
//=================================================
import javax.swing.*;
import java.awt.*;

public class BasicGraphics extends javax.swing.JApplet
{
    public void paint(Graphics g)
    {
        //Set background
        g.setColor(Color.blue);
        g.fillRect(0, 0, 800, 800);
        /* Draw the square. */
        g.setColor(Color.green);
        g.fillRect(60, 60, 200, 200);
        g.setColor(Color.cyan);
        g.fillRect(160, 160, 100, 100);
        g.setColor(Color.blue);
        g.fillRect(160, 160, 50, 50);


    }  // end paint()

	public static void main(String[] args)
    	{
        	JFrame window = new JFrame("Greeeeeen");

      		// Add the drawing canvas and do necessary things to
     		// make the window appear on the screen!
        	window.getContentPane().add(new BasicGraphics());
        	window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        	window.setVisible(true);
		window.setSize(600, 400);

        	//window.pack();
    	}
} // end class BasicGraphics

