//Chris Taylor
//Classwork 10-26-2016

import java.util.Scanner;

public class WhileMain
{
    public static void main (String args [])
    {
        Scanner scan = new Scanner (System.in);
        System.out.println("Enter a positive integer.");
        int count = 0;
        int num = 0;
        num = scan.nextInt();

        //Validating input
        while(num < 0)
        {
            System.out.println("That's not a positive number, dingus. Try again.");
            num = scan.nextInt();
            System.out.println("Iteration "+count+": user input: "+num);

            count++;
        }
        //Making instance of WhyExample
        WhileExample numIterate = new WhyExample(0);
        numIterate.setNum(num);
    }
}
