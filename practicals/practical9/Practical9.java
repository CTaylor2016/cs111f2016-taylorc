//Chris Taylor
//Practical 9
//11-04-2016

import java.util.*;
import java.io.*;

public class Practical9

    {
        public static void main(String[] args) throws IOException
        {
            Scanner scan = new Scanner(System.in);
            System.out.println("Name a file.");
            String scanned = scan.next();
            File file = new File(scanned);
            Scanner in = new Scanner (file);
            int numWords = 0;
            while(in.hasNext()) // continue as long as there is more data
            {
                String x = in.next(); // get next word
                numWords++;
            }
            System.out.println("Number of words: " + numWords);
        }
    }
