//Chris Taylor
//11-06-2016

public class Loops
{
    public static void main (String args[])
    {
        //while loop
        int count = 0;
        while(count<100)
        {
            System.out.println(count);
            count+=10;
        }
        //do while
        count=0;
        do
        {
            System.out.println(count);
            count++;
        }
        while(count<5);
        //for loop
        for(count = 0; count<=10; count++)
            System.out.println(count);
    }
}
