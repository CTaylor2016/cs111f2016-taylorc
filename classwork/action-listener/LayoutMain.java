import java.awt.*;
import javax.swing.*;

public class LayoutMain
{
    JFrame frame = new JFrame();
    frame.setLayout(new FlowLayout());

    frame.add(new JButton("Button 1"));
    frame.add(new JLabel("Label 2"));
    frame.add(new JTextField("Text Field 1"));

    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JPanel panel = new JPanel();
    panel.setLayout(new GridLayout(0, 1));

    panel.add(new JButton("Button 2"));
    panel.add(new JLabel("Label 2"));
    panel.add(new JTextField("Text Field 2"));

    frame.add(panel);

    frame.pack();
    frame.setVisible(true);
}
