//*******************
// CMPSC 111, Class Exercise
// November 14, 2016
//
// Chris Taylor
// Purpose: Demonstrate the usage of loops and arrays to solve a birthday problem
//*********************
import java.io.*;
import java.util.Scanner;

public class BirthdayProblem
{
	public static void main(String args[]) throws IOException
	{

		String [] used = new String[29];

		File file = new File("birthdays.txt");
		Scanner input = new Scanner(file);

		// read from the file and save values into an array
		String [] bd = new String[29];
		int count = 0;

		while(input.hasNext())
		{
			String birthday = input.next();
			bd[count] = birthday;
			count++;
		}

		// iterate through the file and find the same birthday
		int i = 0, j=0;
		while(i<bd.length)
		{
		   while(j<used.length){
			if(bd[i].equalsIgnoreCase(used[j]))
			{
				System.out.println("*** Duplicate Birthday: "+bd[i]);
				break;
			}
		       j++;
		    }
		    used[i] = bd[i];
		   System.out.println("Birthday: "+used[i]);
		   i++;
		   j=0;
		}
	}
}
