//*********************************************************************************
// CMPSC 111 Fall 2016
// Practical 10
//
// Purpose: Program demonstrating reading input from the terminal
//*********************************************************************************
import java.util.Scanner;

public class SwitchDay
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.print( "Enter a day of the week: "  );
        String day = s.next();

        switch(day)
        {
            case "Monday": case "Tuesday": case "Wednesday": case "Thursday": case "Friday": System.out.println("weekday");
                           break;
            case "Saturday": case "Sunday": System.out.println("weekend");
                           break;
            default: System.out.println("Not a day");

        }
    }
}
