/************************************
  Honor Code: This work is mine unless otherwise cited.
  Chris Taylor
  CMPSC 111
  30 September 2016
  Practical 4

  Basic Input with a dialog box
 ************************************/

import javax.swing.JOptionPane;
import java.util.Random;

public class Practical4
{
    public static void main ( String[] args )
    {
        int age = 0; // user's input - age
        int age1 = 20; // modified age
        int salary; //user's input for salary
        int salary1; //modified salary

        Random random = new Random();

        // display a dialog with a message
        JOptionPane.showMessageDialog( null, "You need to go under cover. No, you don't have a choice in the matter." );

        // prompt user to enter the first name
        String name = JOptionPane.showInputDialog("Please enter your first name.");

        //prompt user to enter last name
        String lastname = JOptionPane.showInputDialog("Please enter your surname.");

        //create a message with the modified name
        String newName = "Your new name is "+ name+"enko "+lastname+"son IV.";

        //display the message with the modified user's name
        JOptionPane.showMessageDialog(null, newName);

        // TO DO: prompt the user to get the last name, modify the last name and display the new last name
        // Done

        // prompt user to enter the age
        age = Integer.parseInt(JOptionPane.showInputDialog("Enter your age, or at least the age you look."));

        // modify the age
        age1 = age+random.nextInt(20)-10;
        String newAge = "Your new age is "+age1+". You should be able to pass for that.";

        // display a message with the new age
        JOptionPane.showMessageDialog(null, newAge);

        //Request pet's name
        String petname = JOptionPane.showInputDialog("What is your pet's name?");

        //Modify pet's name
        String newPet = "You need to address your pet as 'Big "+petname.charAt(0)+"' from now on. This is vital to your cover story.";

        //display new pet name
        JOptionPane.showMessageDialog(null, newPet);

        salary = Integer.parseInt(JOptionPane.showInputDialog("Don't worry, you're getting compensated for your trouble. How much do you make a year right now, in dollars?"));

        salary1 = salary + ((random.nextInt(20)+5)*salary/100);

        String newSalary = "Wait, really? Sheesh, you needed a raise anyway. Starting now, you're making "+salary1+" a year.";

        JOptionPane.showMessageDialog(null, newSalary);

        // TO DO: modify the above questions/answers to your own
        // Done
        // TO DO: come up with your own (at least three) questions and answers
        // 1 done

    } //end main
}  //end class Practical4
