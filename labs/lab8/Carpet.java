
//***************
//Honor Code: This work is mine unless otherwise cited.
//Chris Taylor
//CMPSC 111 Fall 2016
//Lab 8
//Date: 11-10-2016
//
//Purpose: To create a symmetrical "carpet" using for and do while loops
//***************
import java.util.Date;
import java.util.Scanner;

public class Carpet
{
	//main method: program execution begins here
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Chris Taylor\n Lab 8\n" + new Date() + "\n");

        //Creating scanner
        Scanner scan = new Scanner(System.in);

        //Having user select character
        System.out.println("Choose a character from which to make your carpet.");
        char c = scan.next().charAt(0);

        //Declaring variables I use to format carpet
        int x;
        int y = 0;

        while(y<32) //Making program repeat so I don't need to copy-paste the lower half of lines.
        {
        while(y==0 || y==31)
        {
            for (x = 0; x<32; x++)
            {
                System.out.print(c);
            }
            System.out.println(c);
            y++;
        }
        while(y==1 || y==30)
        {
            System.out.print(c);
            for (x=1; x<4; x++)
            {
                System.out.print(" ");
            }
            for (x=4; x<15; x++)
            {
                System.out.print(c);
            }
            for (x=15; x<18; x++)
            {
                System.out.print(" ");
            }
            for (x=18; x<29; x++)
            {
                System.out.print(c);
            }
            for (x=29; x<32; x++)
            {
                System.out.print(" ");
            }
            System.out.println(c);
            y++;
        }
        while(y==2 || y==29)
        {
            System.out.print(c);
            for (x=1; x<4; x++)
            {
                System.out.print(" ");
            }
            for (x=4; x<14; x++)
            {
                System.out.print(c);
            }
            for (x=14; x<19; x++)
            {
                System.out.print(" ");
            }
            for (x=19; x<29; x++)
            {
                System.out.print(c);
            }
            for (x=29; x<32; x++)
            {
                System.out.print(" ");
            }
            System.out.println(c);
            y++;
        }
        while(y==3||y==28)
        {
            System.out.print(c);
            for (x=1; x<4; x++)
            {
                System.out.print(" ");
            }
            for (x=4; x<13; x++)
            {
                System.out.print(c);
            }
            for (x=13; x<20; x++)
            {
                System.out.print(" ");
            }
            for (x=20; x<29; x++)
            {
                System.out.print(c);
            }
            for (x=29; x<32; x++)
            {
                System.out.print(" ");
            }
            System.out.println(c);
            y++;
        }
        while(y==4||y==27)
        {
            for (x=0; x<5; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for (x=6; x<12; x++)
            {
                System.out.print(c);
            }
            for(x=12; x<21; x++)
            {
                System.out.print(" ");
            }
            for(x=21; x<28; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for (x=29; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==5||y==26)
        {
            for (x=0; x<6; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for (x=7; x<11; x++)
            {
                System.out.print(c);
            }
            for(x=11; x<22; x++)
            {
                System.out.print(" ");
                System.out.print(c);
                x++;
            }
            for(x=22; x<26; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for (x=27; x<32; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==6||y==25)
        {
            for (x=0; x<7; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for (x=8; x<10; x++)
            {
                System.out.print(c);
            }
            for(x=10; x<23; x++)
            {
                System.out.print(" ");
                System.out.print(c);
                x++;
            }
            for(x=23; x<25; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for (x=26; x<32; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==7||y==24)
        {
            for (x=0; x<8; x++)
            {
                System.out.print(c);
            }
            System.out.print(" ");
            for(x=10; x<23; x++)
            {
                System.out.print(" ");
                System.out.print(c);
                x++;
            }
            System.out.print(" ");
            System.out.print(" ");
            for (x=25; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==8||y==23)
        {
            for (x=0; x<8; x++)
            {
                System.out.print(c);
            }
            for(x=9; x<27; x++)
            {
                System.out.print(" ");
                System.out.print(c);
                x++;
            }
            for (x=26; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==9||y==22)
        {
            for (x=0; x<7; x++)
            {
                System.out.print(c);
            }
            for(x=7; x<26; x++)
            {
                System.out.print(" ");
            }
            for (x=26; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==10||y==21)
        {
           for (x=0; x<6; x++)
            {
                System.out.print(c);
            }
            for(x=6; x<27; x++)
            {
                System.out.print(" ");
            }
            for (x=27; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==11||y==20)
        {
            for (x=0; x<5; x++)
            {
                System.out.print(c);
            }
            for(x=5; x<28; x++)
            {
                System.out.print(" ");
            }
            for (x=28; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        //Haven't finished past here
        while(y==12||y==19)
        {
            for (x=0; x<4; x++)
            {
                System.out.print(c);
            }
            for(x=4; x<29; x++)
            {
                System.out.print(" ");
            }
            for (x=29; x<33; x++)
            {
                System.out.print(c);
            }
            System.out.print("\n");
            y++;
        }
        while(y==13||y==18)
        {
            for (x=0; x<3; x++)
            {
                System.out.print(" ");
            }
            for(x=3; x<30; x++)
            {
                System.out.print(c);
            }
            for (x=30; x<33; x++)
            {
                System.out.print(" ");
            }
            System.out.print("\n");
            y++;
        }
        while(y==14||y==17)
        {
            for (x=0; x<2; x++)
            {
                System.out.print(" ");
            }
            for(x=2; x<31; x++)
            {
                System.out.print(c);
            }
            for (x=31; x<33; x++)
            {
                System.out.print(" ");
            }
            System.out.print("\n");
            y++;
        }
        while(y==15||y==16)
        {
            for (x=0; x<1; x++)
            {
                System.out.print(" ");
            }
            for(x=1; x<32; x++)
            {
                System.out.print(c);
            }
            for (x=32; x<33; x++)
            {
                System.out.print(" ");
            }
            System.out.print("\n");
            y++;
        }
        }
    }
}
