//Chris Taylor
//10-26-2016

public class WhileExample
{
    //instance
    private int num;

    //constructor
    public WhileExample(int n)
    {
        num = n;
    }

    //set method
    public void setNum(int n)
    {
        int count = 0;
        while(count<1000)
        {
            num+=n;
            System.out.println("Iteration "+count+": number is "+num);
            count++;
        }
    }
}
