// Create a class GradeBookTest to instantiate GradeBook class

public class GradeBookTest {
    System.out.println("Start of main method");
    public static void main (String args[]) {
        GradeBook myGradeBook  = new GradeBook ();
        System.out.println("After creation of instance");
        myGradeBook.displayMessage();
        System.out.println("After calling displayMessage");
    }
}
