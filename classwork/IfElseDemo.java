/* CMPSC 111 Fall 2016
  Janyl Jumadinova
  Class Example
  October 5, 2016

  Purpose: To determine whether a grade a vowel
*/

import java.util.Scanner;
public class IfElseDemo
{
 	public static void main ( String args[] )
 	{
		Scanner input = new Scanner ( System.in );
 		System.out.print ( "Enter a character to test: " );
 	 	char character;	  	       		// char data type
 		character = input.next().charAt(0); 	// get character from input
 	 	if (character == 'a' || character == 'A' || character == 'e' || character=='E' || character == 'i' || character=='I' || character == 'o' || character == 'O' || character == 'u' || character=='U')
 			System.out.println ( character+" is a vowel." );
 		else
 			System.out.println ( character+" is not a vowel." );
 	}
}

