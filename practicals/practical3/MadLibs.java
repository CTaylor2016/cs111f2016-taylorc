//Honor Code: This work is mine unless otherwise cited.
//Chris Taylor
//CMPSC111 Fall 2016
//09/16/2016
//Practical 3

import java.util.Scanner;

public class MadLibs
{
	public static void main(String[] args)
	{
		//String declarations
		String word1;
		String word2;
		String word3;
		String word4;
		String word5;

		Scanner userInput = new Scanner (System.in);

		System.out.println("Hello! We're gonna play Mad Libs!\n" + "First, tell me a name:");
		word1 = userInput.nextLine();
		System.out.println("Now an adjective:");
		word2 = userInput.nextLine();
		System.out.println("A noun:");
		word3 = userInput.nextLine();
		System.out.println("A number larger than one:");
		word4 = userInput.nextLine();
		System.out.println("And finally, something frightening:");
		word5 = userInput.nextLine();
		System.out.println("Alright! It's time to actually tell this story!\n" + "Once, long ago, way out in the woods, " + word1 + " lived all on their own. They weren't lonely, though; they were very happy out there, with no one bothering them. For example, no one asked why all their possessions were " + word2 + ". If anyone did ask, they would have just said that they really liked " + word2 + " things, but they liked not being bothered with stupid questions.\n" + "In fact, about the only thing they owned that wasn't " + word2 + " was their " + word3 + ". They kind of wished it was " + word2 + ", too, but they loved it too much to try and change it.\n" + "One day, though, something terrible happened. A swarm of " + word5 + "s came to " + word1 + "'s door and demanded that they give the awful " + word5 + "s their home! " + word1 + " thought about fighting them, but then they realized there had to be at least " + word4 + " " + word5 +"s. So, with a heavy heart, they abandoned their haven and all their " + word2 + " things, with only their " + word3 + " and the clothes on their back.\n" + "\n" + "What? You wanted a happy story? Then you shouldn't have asked for one about " + word1 + "!");
	}
}
