//Chris Taylor
//Classwork 09-14-16

import java.util.Scanner;

public class Bork
{
	public static void main (String[] args)
	{
		double age;
		age = 20;

		double result = 0; //declaration and assignment in one line

		//declare Scanner object
		Scanner userInput = new Scanner(System.in);

		//prompt for input
		System.out.println("Enter an age: ");
		age = userInput.nextDouble();
		System.out.println("You entered: "+age);
		//convert to minutes
		result = age*365*24*60;
		System.out.println("Age in minutes: "+result);
	}
}
