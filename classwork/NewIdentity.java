//==========================================
// Chris Taylor
// Class Exercise
// September 28, 2016
//
// Purpose: This program helps the user to create
// a new random identity
// ==========================================
import java.util.Scanner;
import java.util.Random;

public class NewIdentity
{

	public static void main(String args[])
   	{
        String firstName, lastName, job;
        int age;
        float salary = 100000;

        // create an instance of the Scanner
        Scanner scan = new Scanner (System.in);

        // create an instance of the Random class
        Random rand = new Random();

        // get the user's input
        System.out.print("Please enter a first name: ");
        firstName = scan.nextLine();
        System.out.print("Please enter a last name: ");
        lastName = scan.nextLine();
        System.out.print("Please enter your dream job: ");
        job = scan.nextLine();
        System.out.print("Please enter an integer (your age): ");
        age = scan.nextInt();
        System.out.print("Please enter a number (your salary): ");
        salary = scan.nextFloat();

		// randomly change the user's input
		// get the length of the firstName string
		int length = firstName.length();

		// get the position of the character in the firstName string
		int position = rand.nextInt(length);
        System.out.println("Position selected is "+position);
        char randomLetter = firstName.charAt(position);
        System.out.println("The random letter is "+randomLetter);
		// pick a character at that position

		// replace all of the selected characters with character 'a'
        firstName=firstName.replace(randomLetter, 'a');

		// append "ov" to the lastName string
        lastName=lastName.concat("ov");
		// change the job to upper case
        job = job.toUpperCase();
		// assign a value for age
        age = rand.nextInt(11)+age-5;
		// assign a value for salary
        salary = salary*(1+(rand.nextInt(41)-20)/100);
		System.out.println("Your new name is "+firstName+" " +lastName+", and you are "+age+" years old. \nYou work as a "+job+ " making $"+salary+ " a year.");

   }
}
