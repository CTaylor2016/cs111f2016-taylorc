//Chris Taylor
//Practical 9
//11-04-2016

import java.util.*;
public class DistinctWords
    {
        public static void main(String[] args)
        {
            ArrayList<String> words = new ArrayList<String>();
            Scanner in = new Scanner(System.in);
            while(in.hasNext())
            {
                String x = in.next().toLowerCase(); // ignore case
                if(!words.contains(x)) //see if x is already in the list of words; if not, add it
                {
                    words.add(x);
                }
            }
            System.out.println("Number of distinct words: " + words.size());
        }
    }
