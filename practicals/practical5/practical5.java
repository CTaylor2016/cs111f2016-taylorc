/************************************
  Honor Code: This work is mine unless otherwise cited.
  Chris Taylor
  CMPSC 111
  7 October 2016
  Practical 5

  Basic Input with a dialog box
 ************************************/

import java.util.Scanner;

public class practical5
{
    public static void main(String[] args)
    {
        int year = 0; //variable declaration

        Scanner userInput = new Scanner(System.in); //scanner declaration

        System.out.println("Enter a year between 0 and 4000 CE."); //input request
        year = userInput.nextInt(); //year re-declaration
        System.out.println("You entered "+year+"."); //response confirming input
        if (year>=0 && year<=4000) //making sure the user does what they're told
        {
            System.out.println("That year is, in fact, between 0 and 4000. Congratulations."); //praising the user for knowing how counting works
            if (year%100==0 && year%400==0 || year%100!=0 && year%4==0) System.out.println("That's a leap year."); //stating the conditions for a leap year
            else System.out.println ("That's not a leap year.");
            if ((year-2013)%17==0) System.out.println("The 17-year cicadas emerge that year."); //stating the conditions for cicadas
            else System.out.println ("No cicadas that year -- what a pity.");
            if ((year-2013)%11==0) System.out.println("The sun is particularly active in "+year+"."); //stating the conditions for sunspots
            else System.out.println("That year will not be a sunspot high.");
        }
        else System.out.println("That year isn't an option. Can't you read?"); //chastising the user for not doing what they were told like a good little puppet
    }
}
