//====================================
// CMPSC 111
// Practical 8
// 28 October 2016
//
// This program describes an octopus in the kitchen.
//====================================

import java.util.Date;

public class Practical8
{
    public static void main(String[] args)
    {
        System.out.println("Janyl Jumadinova\n" + new Date() + "\n");

        // Variable dictionary:
        Octopus ocky;           // an octopus
        Octopus bob; //another octopus
        Utensil spat;           // a kitchen utensil
        Utensil whisk; //another utensil

        spat = new Utensil("spatula"); // create a spatula
        spat.setColor("green");        // set spatula properties--color...
        spat.setCost(10.59);           // ... and price

        whisk = new Utensil("whisk"); //create a whisk
        whisk.setColor("purple"); //set color
        whisk.setCost(4.50); //set price

        ocky = new Octopus("Ocky", 25);    // create and name the octopus
        //ocky.setAge(10);               // set the octopus's age...
        ocky.setWeight(100);           // ... weight,...
        ocky.setUtensil(spat);         // ... and favorite utensil.

        bob = new Octopus ("Bob", 7900); //create, name, set age of octopus 2
        bob.setWeight(548); //set weight
        bob.setUtensil(whisk); //set utensil

        System.out.println("Testing 'get' methods:");
        System.out.println(ocky.getName() + " weighs " +ocky.getWeight()
            + " pounds\n" + "and is " + ocky.getAge()
            + " years old. His favorite utensil is a "
            + ocky.getUtensil());

        System.out.println(ocky.getName() + "'s " + ocky.getUtensil() + " costs $"
            + ocky.getUtensil().getCost());
        System.out.println("Utensil's color: " + ocky.getUtensil().getColor() + "\n");

        //Printing Bob's stuff

         System.out.println("Testing 'get' methods:");
        System.out.println(bob.getName() + " weighs " +bob.getWeight()
            + " pounds\n" + "and is " + bob.getAge()
            + " years old. His favorite utensil is a "
            + bob.getUtensil());

        System.out.println(bob.getName() + "'s " + bob.getUtensil() + " costs $"
            + bob.getUtensil().getCost());
        System.out.println("Utensil's color: " + bob.getUtensil().getColor());


        // Use methods to change some values:

        ocky.setAge(20);
        ocky.setWeight(125);
        spat.setCost(15.99);
        spat.setColor("blue");

        System.out.println("\nTesting 'set' methods:");
        System.out.println(ocky.getName() + "'s new age: " + ocky.getAge());
        System.out.println(ocky.getName() + "'s new weight: " + ocky.getWeight());
        System.out.println("Utensil's new cost: $" + spat.getCost());
        System.out.println("Utensil's new color: " + spat.getColor());
    }
}
