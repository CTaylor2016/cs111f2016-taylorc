import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Dialogue

{
    private Resources numbers;

    //variables
    int[] oldVariables = new int[9];
    //frame stuff
    JFrame news = new JFrame();
    JFrame help = new JFrame();
    JLabel message = new JLabel();
    JLabel message2 = new JLabel();

    public Dialogue(Resources stuff)
    {
        news.setLayout(new GridLayout(2,1));
        news.setSize(500,400);
        news.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        news.setLocationRelativeTo(null);
        news.add(message);
        numbers=stuff;
    }

    public void saveNums()
    {
        oldVariables[0]=numbers.getAmmo();
        oldVariables[1]=numbers.getFood();
        oldVariables[2]=numbers.getHealth();
        oldVariables[3]=numbers.getZombies();
        oldVariables[4]=numbers.getWalls();
        oldVariables[5]=numbers.getMaterial();
        oldVariables[6]=numbers.getHours();
        oldVariables[7]=numbers.getDays();
        oldVariables[8]=numbers.getDanger();
    }

    public void helpWindow()
    {
        message.setText("<html>"+"This game is a resource management simulator. Your goal is to survive as long as possible without your health hitting 0. \n"+"You need to manage your health, the strength of your walls, and the zombie population. \n"+"The 'Gather' buttons may get you more of their respective supplies, but also put you at risk of being hurt. The amount of damage you take scales with the current zombie population. \n"+"The 'Hunt' button expends some of your ammo in exchange for destroying some zombies. \n"+"The 'Eat' button uses up a day's worth of rations in exchange for healing you at any time. \n"+"The 'Fortify' button expends some of your resources to strengthen your wall. All these actions take 2 hours."+"</html>");
        news.add(message2);
        message2.setText("<html>"+"Every night, starting at 18:00, the zombies attack your encampment. As long as your walls are standing, they can't attack you directly. As time passes, it becomes harder to find supplies, and more zombies are drawn towards the evidence of your presence. You can find a new place to make camp in order to lose their attention and find more supplies, but you can only carry a finite amount of supplies, there will be no walls when you arrive, and there will most likely already be a small zombie population. Moving takes 6 hours."+"</html>");
        news.setVisible(true);
    }

    public void gameOver()
    {
        news.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        message.setText("<html>"+"You died. You lasted "+oldVariables[7]+" days."+"</html>");
        news.setVisible(true);
    }

    public void zKill()
    {
        news.remove(message2);
        message.setText("<html>"+"You expended "+(oldVariables[0]-numbers.getAmmo())+" rounds of ammo and killed "+(oldVariables[3]-numbers.getZombies())+" zombies."+"</html>");
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

    public void ammoGet()
    {
        news.remove(message2);
        message.setText("<html>"+"You found "+(numbers.getAmmo()-oldVariables[0])+" rounds of ammo and lost "+(oldVariables[2]-numbers.getHealth())+" health."+"</html>");
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

    public void foodGet()
    {
        news.remove(message2);
        if(numbers.getFood()!=oldVariables[1])
        {
            message.setText("<html>"+"You found a day's worth of food and lost "+(oldVariables[2]-numbers.getHealth())+" health."+"</html>");
        }
        else
        {
            message.setText("<html>"+"You found no food and lost "+(oldVariables[2]-numbers.getHealth())+" health."+"</html>");
        }
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

    public void foodEat()
    {
        news.remove(message2);
        if(numbers.getHealth()==100)
        {
            message.setText("<html>"+"You're in perfect condition!"+"</html>");
        }
        else
        {
            message.setText("<html>"+"You ate a day's worth of rations and regained "+(numbers.getHealth()-oldVariables[2])+" health."+"</html>");
        }
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

    public void buildGet()
    {
        news.remove(message2);
        message.setText("<html>"+"You found "+(numbers.getMaterial()-oldVariables[5])+" pounds of building material and lost "+(oldVariables[2]-numbers.getHealth())+" health."+"</html>");
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

    public void wallBuild()
    {
        news.remove(message2);
        message.setText("<html>"+"You used "+(oldVariables[5]-numbers.getMaterial())+" pounds of material to improve your walls by "+(numbers.getWalls()-oldVariables[4])+" arbitrary units of resilience."+"</html>");
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

    public void campFind()
    {
        news.remove(message2);
        message.setText("<html>"+"You managed to find a suitable place to camp. You had to leave behind "+(oldVariables[0]-numbers.getAmmo())+" rounds of ammunition, "+(oldVariables[1]-numbers.getFood())+" days' worth of food, and "+(oldVariables[5]-numbers.getMaterial())+" pounds of building material. There are "+(numbers.getZombies())+" zombies in the area."+"</html>");
        news.setVisible(true);
        saveNums();
        if(numbers.getHealth()<1)
        {
            gameOver();
        }
    }

}
