
//***************
//Honor Code: This work is mine unless otherwise cited.
//Chris Taylor
//CMPSC 111 Fall 2016
//Practical 2
//Date: 09-09-2016
//
//Purpose: Print something "interesting"
//***************
import java.util.Date;

public class Practical2
{
	//main method: program execution begins here
	public static void main(String[] args)
	{
		//Label output with name and date:
		System.out.println("Chris Taylor\n Practical 2\n" + new Date() + "\n");
		//The hat
		System.out.println("    _________\n"+"    |VVVVVVV|\n"+"    |VVVVVVV|\n"+"    |VVVVVVV|\n"+"    |_______|    ");
		//The face
		System.out.println("     | 0 0 |\n"+"     |     |\n"+"     | \\_/ |\n"+"     \\_____/");
		//The shoulders
		System.out.println("/‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾\\\n"+"|               |\n"+"|               |\n"+"|  |         |  |\n"+"|  |         |  |\n");
		System.out.println("\n\nDo you like my hat?");
	}
}
