import javax.swing.*;
import java.awt.*;

public class ActionListenerMain
{
    public static void main(String args[])
    {
        //Create frame/window
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Arranges L-to-R
        frame.setLayout(new FlowLayout());

        //create a button
        JButton myButton = new JButton("My Button");

        //Create textfield
        JTextField myTextField = new JTextField("Type here");

        //create label
        JLabel myLabel = new JLabel("My label");

        myButton.addActionListener(new SetTextListener(myTextField, myLabel));

        frame.add(myButton);
        frame.add(myTextField);
        frame.add(myLabel);

        frame.setVisible(true);
    }
}
